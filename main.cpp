#include <iostream>
#include"DS3231.h"
#include<cstring>
#include<stdio.h>
#include<fcntl.h>
#include<sys/ioctl.h>
#include<unistd.h>
#include<linux/i2c-dev.h>
#include<string>
#define BUFFER_SIZE 19      //0x00 to 0x12
using namespace std;

int main() {
	DS3231 rtc; 
	char hour;
	hour = 2;
	char min;
	min = 3;

	cout<<"Printing current time and date to console"<<endl;
	rtc.printTime();
	rtc.printDate();


	cout<<"printing current temperature"<<endl;
	rtc.printTemp();


	cout<<"printing current alarm time"<<endl;
	rtc.printAlarm();

	cout<<"Attempting to  change time to 10:15"<<endl;
	rtc.changeHour(hour);
	rtc.changeMin(min);
        rtc.printTime();

	cout<<"attempting to set date to 1/3/2020"<<endl;	
	rtc.setDate(20,3,1);
	rtc.printDate();

	cout<<"setting alarm to 10:15"<<endl;
	rtc.setAlarmHour(hour);
	rtc.setAlarmMin(min);
	rtc.printAlarm();
	return 0;
}
